/*
  sms.db8 - Open Source assistant for migration of SMS from db3 to db8
  Author: rcmarvin
  Web: https://bitbucket.org/rcmarvin/sms.db8
  Version: 0.4.7

  The MIT License - Copyright (c) 2011 
*/


if (!Array.isArray)
{
	Array.isArray = Array.isArray || function(o) { return Object.prototype.toString.call(o) === '[object Array]'; };
}

var sms=(function(){

	var people,messages;	
	var phones={};
	var phoneNumberLength=[];
	var ct_old, ct_current;
	var personList={}, phoneList={};
	var ctMap={};
	var i,interval;var inProgress=false;
	
	// create a phone database from db8 Person table
	var grabPeoplePhone=function grabPeoplePhone(){
		phones={};
		if (people.results.length>0)
		{
			var i,j,totalPerson,totalPhoneNumber,phone;
			var phoneNumberLengthA={};
			totalPerson=people.results.length;
			log.debug("Total of contact(s): "+totalPerson);
			for (i=0;i<totalPerson;i++)
			{
				totalPhoneNumber=people.results[i].phoneNumbers.length
				if (totalPhoneNumber>0)
				{
					for (j=0;j<totalPhoneNumber;j++)
					{
						/*
							convert normalized phone into normal one:
							normal VS normalized value:
							+852 12345678 -> -87654321--258-+
							38216803 -> -30861283---
						*/
						phone=people.results[i].phoneNumbers[j].normalizedValue.match("^-([*#0-9]+)-")[1].split("").reverse().join("");
						phoneNumberLengthA[phone.length]=undefined;
						phones[phone]={};
						phones[phone]['personId']=people.results[i]._id;
						phones[phone]['phoneNumber']=people.results[i].phoneNumbers[j];
					}
				}
			}
			for (i in phoneNumberLengthA)
			{
				phoneNumberLength.push(i);
			}
			// large value stored first
			phoneNumberLength.sort(function(a,b){return b-a;})
		}
	};
	
	var normalizeAddress=function normalizeAddress(replyAddress){
		// assume normalizeAddress should have maximum of eight in length
		// (which is probably wrong, should varies in different region)
		var address=String(replyAddress).replace(/ /g,'').replace(/-/g,'');
		var len=address.length;
		if (len > 8)
			return address.substring(len, len-8);
		else
			return address;
	}
	// return relevant personId with a phone number
	var getPersonFromPhone=function getPersonFromPhone(phone){
		var len=phoneNumberLength.length;
		var pLen=phone.length;
		var pp='';
		for (var i=0;i<len;i++)
		{
			if (pLen >= phoneNumberLength[i])
			{
				pp=phone.substring(pLen, pLen-phoneNumberLength[i]);
				if (pp in phones && phones[pp] !== null)
				{
					return {'status': 'successful', 'personId': phones[pp]['personId'], 'phoneNumber': phones[pp]['phoneNumber']};
				}
			}
		}
		return {'status': 'fail', 'phone': phone};
	};
	// prepare personId db and phone db used by current chatThread
	var grabCCInfo=function grabCCInfo(){
		var ctTotal=ct_current.results.length;
		var ct,i;
		personList={};phoneList={};
		for (i=0;i<ctTotal;i++)
		{
			ct=ct_current.results[i];
			if (ct.personId)
			{
				personList[ct.personId]=ct._id;
			}
			else if (ct.replyService==="sms")
			{
				// replyAddress should always be phone;
				// when IM account is removed, IM thread will also be removed
				// thus IM message should be related with personId
				phoneList[ct.replyAddress]=ct._id;
			}
		}
	};

	// from http://0xcc.net/jsescape/
	// used to avoid non-ascii character in shell script
	var escapeToHTML=function escapeToHTML(string){
	  var escaped = ''
	  for (var i = 0; i < string.length; ++i) {
	    var hex = string.charCodeAt(i).toString(16).toUpperCase();
	    escaped += "&#x" + "0000".substr(hex.length) + hex + ";";
	  }
	  return escaped;
	};
	var convertToUTF16=function convertToUTF16(string){
		return string.replace(/&#x([A-F0-9]{4});/g,"\\u$1");
	};
	
	// convert old db3 chatThread to new db8 format
	var processCT=function processCT(){
		grabCCInfo();
		var ct,i;
		for (i=0;i<ct_old.length;i++)
		{
			ct=ct_old[i];
			if (! ( ct.$type === "SMS" || ct.$type === "PHONE" ))
			{
				// IM thread will be ignored
				// but even when thread contain SMS, the address may not contain phone number
				// because if the last msg is from IM, the address will only contain IM identity
				// so will try the best to locate person using alternate address
				if ( ct.$type==="IM" && ct.$chatAddress!=="" )
				{
					log.debug("Mixed-type thread detected: ("+ct.$chatAddress+")");
					ct.replyAddress=ct.$chatAddress;
				}
				else
				{
					log.debug ("IM thread detected ("+ ct.replyAddress +")");
					ct_old.splice(i,1);i--;
					continue;
				}
			}
			delete ct.$chatAddress;
			ct.normalizedAddress=normalizeAddress(ct.replyAddress);
			ct.replyService="sms";
			delete ct.$type;
			delete ct.$cfid;
			
			var result=getPersonFromPhone(ct.normalizedAddress);
			if (result.status==="successful")
			{
				if (result.personId in personList)
				{
					// person already exist in db8 ChatThread
					// great! then we can save our work of inserting new record
					log.debug("Chat thread of "+ct.displayName+" already contain in db8");
					ct_old.splice(i,1);i--;
					continue;
				}
				ct.personId=result.personId;
				// as long as a personId exist for a chatthread, there seems no harm to remove normalizedAddress
				// until a proper solution for normalizeAddress come out, do not do it 
				delete ct.normalizedAddress;
			}
			else
			{
				delete ct.personId;
				if (ct.replyAddress in phoneList)
				{
					// db8 ChatThread already have thread with that phone number
					// only phone with exact same number will be matched
					log.debug("Chat thread of "+ct.replyAddress+" already contain in db8");
					ct_old.splice(i,1);i--;
					continue;
				}
			}
			delete ct.$name;
			if (ct.$flags < 128)
			{
				ct.flags.outgoing=false;
				ct.flags.visible=true;
			}
			else if (ct.$flags > 127)
			{
				ct.flags.outgoing=true;
				ct.flags.visible=true;
			}
			delete ct.$flags;
			ct.summary=escapeToHTML(ct.summary);
			ct.displayName=escapeToHTML(ct.displayName);
			ct_old[i]=ct;
		}
		return ct_old;
	};

	var checkCT=function checkCT(){
		grabCCInfo();
		var ctTotal=ct_old.length;
		var ct,i;
		ctMap={};
		
		for (i=0;i<ctTotal;i++)
		{
			ct=ct_old[i];
			if (! ( ct.$type === "SMS" || ct.$type === "PHONE" ))
			{
				if ( ct.$type==="IM" && ct.$chatAddress!=="" )
					ct.replyAddress=ct.$chatAddress;
				else
					continue;
			}
			ct.normalizedAddress=normalizeAddress(ct.replyAddress);

			var result=getPersonFromPhone(ct.normalizedAddress);
			if (result.status==="successful")
			{
				if (result.personId in personList)
				{
					// thread with personId already exist in db8 ChatThread
					ctMap[ct.$cfid]=personList[result.personId];
					log.debug("chat thread id db3:"+ct.$cfid+" => db8:"+personList[result.personId]);
					continue;
				}
			}
			else
			{
				if (ct.replyAddress in phoneList)
				{
					// db8 ChatThread already have thread with that phone number
					ctMap[ct.$cfid]=phoneList[ct.replyAddress];
					log.debug("chat thread id db3:"+ct.$cfid+" => db8:"+phoneList[ct.replyAddress]);
					continue;
				}
			}
			alert("Cannot match old ChatThread with current db8 ChatThread");
			log.error("Error on thread with address="+ct.replyAddress);
			return false;
		}
		return true;
	};
	// convert message from db3 to db8 format
	var processMessages=function processMessages(){
		if (inProgress)
			return;
		inProgress=true;
		var date=new Date();
		var messageTotal=messages.length;
		var b;
		if (typeof i === "undefined")
			i=0;
		for (;i<messageTotal;i++)
		{
			// try to avoid long time processing that cause Firefox to popup alert message
			if (i % 100 === 0)
			{
				if (new Date()-date > 1000)
				{
					log.debug('timeout, wait for next iteration');
					/*
					var self = arguments.callee;
					setTimeout(function(){self(i);}, 250);
					*/
					inProgress=false;
					return;
				}
			}
			b=messages[i];
			if (b.$cfid in ctMap)
			{
				b.conversations[0]=ctMap[b.$cfid];
				delete b.$cfid;
			}
			else
			{
				// SMS that cannot be mapped from old thread to new thread will be ignored
				// probably caused by SMS related to chatThread with Type=IM
				log.warn('Message ignored, with cfid='+b.$cfid);
				continue;
			}
			if (b.$status<2)
				b.status="successful";
			else
			{
				b.status="permanent-fail";
				b.errorCategory="";
			}
			delete b.$status;
			if (b.$smsClass===2) /* sms received */
			{
				b.callbackNumber = "";
				b.flags.read=true;
				b.folder="inbox";
				b.networkMsgId=b.timestamp;
				b.priority=0;
				b.smsType=0;
				b.from = { addr: b.$address};
			}
			else if (b.$smsClass===0) /* sms sent */
			{
				b.flags.visible=true;
				b.flags.read=true;
				b.flags.deliveryReport=false;
				b.folder="outbox";
				b.to = [{ 'addr': b.$address, 'name': b.$address}];
				// it seems that the normalized address will only be added when the number is already in contact list
				// and there may be no harm to leave it out
				// so until the normalizeAddress is working properly, don't do it
				//b.to[0].normalizedAddress=normalizeAddress(b.$address);
			}
			delete b.$address;
			delete b.$smsClass;
			b.messageText=escapeToHTML(b.messageText);
			messages[i]=b;
		}
		clearInterval(interval);
		showMessages();
		inProgress=false;
	};
	// show the shell script in textarea
	var showMessages=function showMessages(){
		var content="cd /media/internal/\n";
		content+="echo 'var ins2Result=[];'>ins2.sh.json.js\n";
		while (messages.length>0)
		{
			content+="\nluna-send -n 1 -a com.palm.app.messaging luna://com.palm.db/put $'"+'{"objects":';
			// avoid "Argument list too long" error when command executed on shell
			// by inserting 100 message at a time
			// the max length should be 2097152, shared by 25 message, each message can have maximum of ~81KiB
			content+=convertToUTF16(JSON.stringify(messages.splice(0,25))).replace(/'/g,"\\'");
			content+='}'+"' | sed -r -e"+' "s/^/ins2Result.push(/" | sed -r -e "s/$/)/" >>ins2.sh.json.js';
		}
		content+="\n\ncat ins2.sh.json.js\n";
		document.getElementById('stepBContent').value=content+"\n";
	};
	
	var processAndShowMessages=function processAndShowMessages(){
		interval=setInterval(processMessages,1250);
	};
	
	return {
		'init': function init(p,m,co,cc){
			people=p;
			messages=m;
			ct_old=co;
			ct_current=cc;
			// ok, we should do some simple checking to ensure those *.json.js is in correct format
			// and avoid alert message that may caused by not existed js file
			var disabled=0;
			if (typeof people==="undefined")
			{
				log.error("persons not defined");
				disabled++;
			}
			else if ( !("results" in people) || typeof people['results']==="undefined")
			{
				alert("[ERROR]persons.json.js => persons in improper format");
				log.error("persons in improper format");
				disabled++;
			}
			else
			{
				if (!Array.isArray(people.results))
				{
					alert("[ERROR]persons.json.js => persons in improper format (non-array)");
					log.error("message in improper format (non-array)");
					disabled++;
				}
				else if (people.results.length < 1)
				{
					log.info("There is no people in contact list.");
				}
			}
			if (typeof messages==="undefined")
			{
				log.error("messages not defined");
				disabled++;
			}
			else
			{
				if (!Array.isArray(messages))
				{
					alert("[ERROR]message.json.js => messages in improper format");
					log.error("message in improper format");
					disabled++;
				}
				else if (messages.length < 1)
				{
					log.warn("There is no message to import.");
					disabled++;
				}
			}
			if (typeof ct_old==="undefined")
			{
				log.error("old chat thread not defined");
				disabled++;
			}
			else
			{
				if (!Array.isArray(ct_old))
				{
					alert("[ERROR]ct_old.json.js => db3 chat thread in improper format");
					log.error("old chat thread in improper format");
				}
				else if (ct_old.length < 1)
				{
					log.warn("There is no chat thread to import.");
					disabled++;
				}
			}
			if (typeof ct_current==="undefined")
			{
				log.error("db8 chatthread not defined");
				disabled++;
			}
			else if ( !("results" in ct_current) || typeof ct_current['results']==="undefined")
			{
				alert("[ERROR]ct_current.json.js => db8 chatthread in improper format");
				log.error("db8 chatthread in improper format");
				disabled++;
			}
			else
			{
				if (!Array.isArray(ct_current.results))
				{
					alert("[ERROR]ct_current.json.js => db8 chatthread in improper format (non-array)");
					log.error("db8 chatthread in improper format (non-array)");
					disabled++;
				}
			}
			
			if (disabled)
			{
				document.getElementById('stepA').disabled=true;
				document.getElementById('stepB').disabled=true;
			}
		},
		'stepA': function stepA(){
			document.getElementById('stepA').disabled=true;
			document.getElementById('stepB').disabled=true;
			grabPeoplePhone();
			processCT();
			var content="";
			if (ct_old.length>0)
			{
				content+="cd /media/internal/\n";
				content+="echo var ins1Result=>ins1.sh.json.js\n";
				content+="luna-send -n 1 -a com.palm.app.messaging luna://com.palm.db/put $'"+'{"objects":';
				content+=convertToUTF16(JSON.stringify(ct_old)).replace(/'/g,"\\'");
				content+='}'+"' >> ins1.sh.json.js";
				content+="\n\ncat ins1.sh.json.js\n";
				content+="\n\n"+"luna-send -n 1 -a com.palm.app.messaging luna://com.palm.db/search '"+'{"query":{"from":"com.palm.chatthread:1"}}'+"'| sed -r -e"+' "s/^/cc=/" > ct_current.json.js'+"\n";
			}
			else
			{
				log.info("No chat thread is required to be inserted. Please proceed with step 3d.");
				alert("No chat thread is required to be inserted.\nPlease proceed with step 3d.");
			}
			return content;
		},
		'stepB': function stepB(){
			document.getElementById('stepA').disabled=true;
			document.getElementById('stepB').disabled=true;
			if (ct_current.results.length < 1)
			{
					alert("[WARN]ct_current.json.js => db8 chatThread should contain some thread such that messages can be inserted.");
					log.warn("db8 chatThread should contain some thread such that messages can be inserted.");
					return;
			}
			grabPeoplePhone();			
			if (checkCT())
			{
				processAndShowMessages();
			}
		}
	};
})();
