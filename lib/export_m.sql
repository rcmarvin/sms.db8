SELECT 'm.push({ "_kind":"com.palm.smsmessage:1", "_sync":true, "conversations": [""], "$smsClass": ' || smsClass || ', "flags": {}, "folder": "", "localTimestamp": ' || deviceTimestamp || ', "messageText": "' || REPLACE(REPLACE(REPLACE(REPLACE(messageText, '\', '\\'), X'0D', ''), X'0A','\n'), '"', '\"') || '", "networkMsgId": ' || r.networkMsgId || ', "serviceName": "sms", "status": "", "$status": ' || status || ', "timestamp": ' || (timeStamp/1000) || ', "$address": "' || r.address || '", "$cfid": ' || ca.has_id || '});' AS json
FROM
com_palm_messaging_data_ChatThread ct
INNER JOIN
com_palm_messaging_data_ChatThread_com_palm_messaging_data_ChatThread_ContactChat_AddressChat ca
	ON ct.id=ca.belongs_id and ct.totalCount>0
INNER JOIN
com_palm_messaging_data_ChatThread_com_palm_pim_FolderEntry_Chat_Messages cf
	ON ct.id=cf.has_id
INNER JOIN
com_palm_pim_FolderEntry f
	ON cf.belongs_id=f.id
INNER JOIN
com_palm_pim_Recipient r
	ON f.id=r.com_palm_pim_FolderEntry_id AND substr(ct.trailingDigits,-5)=substr(r.address,-5)
WHERE f._class_id=14
AND
	f.messageType="SMS"
AND
	ct._class_id=18
ORDER BY f.id;
