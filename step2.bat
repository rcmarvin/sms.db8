@echo off
cd /d "%~dp0"

set novacom=
set sqlite=
set palmdb=
set webosdevice=

if exist "C:\Program Files\Palm, Inc\novacom.exe" set novacom=C:\Program Files\Palm, Inc\novacom.exe
if exist "C:\Program Files(x86)\Palm, Inc\novacom.exe" set novacom=C:\Program Files(x86)\Palm, Inc\novacom.exe
if exist "C:\Program Files\HP webOS\SDK\bin\novacom.exe" set novacom=C:\Program Files\HP webOS\SDK\bin\novacom.exe
if exist "C:\Program Files(x86)\HP webOS\SDK\bin\novacom.exe" set novacom=C:\Program Files(x86)\HP webOS\SDK\bin\novacom.exe

if "%novacom%"=="" call "%~dp0lib\error.bat" [ERROR] novacom not found!
if not "%novacom%"=="" echo [INFO] novacom found = %novacom%

if not "%novacom%"=="" "%novacom%" -l
if not errorlevel 1 set webosdevice=yes
if not "%webosdevice%"=="yes" echo [ERROR] WebOS device not found

if exist lib\sqlite3.exe set sqlite=%~dp0lib\sqlite3.exe
if "%sqlite%"=="" call "%~dp0lib\error.bat" [ERROR] sqlite3 not found!
if not "%sqlite%"=="" echo [INFO] sqlite3 found

if exist PalmDatabase.db3 set palmdb=%~dp0PalmDatabase.db3
if "%palmdb%"=="" call "%~dp0lib\error.bat" [ERROR] PalmDatabase.db3 not found!
if not "%palmdb%"=="" echo [INFO] PalmDatabase.db3 found

if "%sqlite%"=="" goto exit
if "%novacom%"=="" goto exit
if "%palmdb%"=="" goto exit
if "%webosdevice%"=="" goto exit

:export_db3
type "%~dp0lib\export_co.sql" | "%sqlite%" "%palmdb%" > ct_old.json.js
type "%~dp0lib\export_m.sql" | "%sqlite%" "%palmdb%" > message.json.js

:export_db8
"%novacom%" -w put file://media/internal/export_db8.sh < "%~dp0lib\export_db8.sh"
if errorlevel 1 call "%~dp0lib\error.bat" [ERROR] Error transferring export shell script to WebOS device
if errorlevel 1 goto exit
"%novacom%" -w run "file://bin/sh /media/internal/export_db8.sh"
if errorlevel 1 call "%~dp0lib\error.bat" [ERROR] Error occurred when request to run export script
if errorlevel 1 goto exit
"%novacom%" -w get file://media/internal/persons.json.js > "%~dp0persons.json.js"
if errorlevel 1 call "%~dp0lib\error.bat" [ERROR] Cannot retrieve persons.json.js
"%novacom%" -w get file://media/internal/ct_current.json.js.1 > "%~dp0ct_current.json.js"
if errorlevel 1 call "%~dp0lib\error.bat" [ERROR] Cannot retrieve ct_current.json.js

echo "Process completed successfully."

:exit
pause
